import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
Myid;
MyProd;
m = 0;
  constructor(private client: HttpClient, private act:ActivatedRoute, private route: Router) { }

  ngOnInit() {
    this.act.paramMap.subscribe((params: ParamMap) => {
      const id = parseInt(params.get('id'));
      this.Myid = id;
    });
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    this.client.get('http://localhost:2000/api/myblogs/' + this.Myid, {headers}).subscribe(data => {
      this.MyProd = data;
      console.log(data);
    });
  }
  Follow() {
    this.m = 1;
    this.route.navigate(['pro']);
  }
}
