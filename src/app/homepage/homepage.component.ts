import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {log} from "util";
import {AppServiceService} from "../login/app-service.service";
import {MyProfileServiceService} from "../myprofile/my-profile-service.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor(private route: Router, private http: HttpClient, private App: AppServiceService, private Obj: MyProfileServiceService) {
  }
Myvar;
  show;
  m = 0;
  ngOnInit() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    this.http.get('http://localhost:2000/api/getblog', {headers}).subscribe(data => {
      this.Myvar = data;
      console.log(data);
    });
    this.Obj.getFollower().subscribe( data => {
      this.show = data;
      console.log(data);
    });
  }
}

